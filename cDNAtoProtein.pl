#!/usr/bin/perl

use strict;
use warnings;

use Bio::SeqIO;

my $fa_file_in = shift;
my $fa_file_out = shift;

# open decompression pipe
open(my $gzh, "gzcat $fa_file_in |") or die $!;

my $fa_hin = Bio::SeqIO->new(
    -fh => $gzh,
    -format => "fasta",
);

my $fa_hout = Bio::SeqIO->new(
-file => ">$fa_file_out",
-format => "fasta",
);

while (my $cdna = $fa_hin ->next_seq)
{
    my $prot = $cdna->translate(
    -codontable_id => 1, # standard genetic code
    -frame => 0, # reading-frame offset 0
    );
    
    $fa_hout->write_seq($prot);
    
}